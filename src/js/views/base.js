export const elements = {
  searchInput: document.querySelector('.search__field'),
  searchForm: document.querySelector('.search'),
  searchRes: document.querySelector('.results'),
  searchResList: document.querySelector('.results__list'),
  searchResPages: document.querySelector('.results__pages'),
  recipe: document.querySelector('.recipe')
}

export const elStr = {
  loader: 'loader'
}

export const renderLoader = parent => { // make the same loader for multiple doms depends on parent
  const loader = `
    <div class="${elStr.loader}">
      <svg>
        <use href="img/icons.svg#icon-cw"></use>
      </svg>
    </div>
  `

  parent.insertAdjacentHTML('afterBegin', loader)
}

export const clearLoader = () => {
  const loader = document.querySelector('.' + elStr.loader)
  if (loader) {
    loader.parentElement.removeChild(loader)
  }
}
