import { elements } from './base.js'

// named export
export const getInput = () => elements.searchInput.value

export const clear = () => {
  elements.searchInput.value = ''
  elements.searchResList.innerHTML = ''
  elements.searchResPages.innerHTML = ''
}

export const highlightSelected = id => {
  const nodes = Array.from(document.querySelectorAll(`.results__link`))
  nodes.forEach(node => {
    node.classList.remove('results__link--active')
  })
  document.querySelector(`a[href*="#${id}"]`).classList.add('results__link--active')
}

// 'pasta tomato and curry'
const limitTitle = (title, limit = 20) => { // 20 will be overwritten by the value supplied when calling the fn
  const newTitle = []

  if (title.length > limit) {
    title.split(' ').reduce((a, b) => {
      if (a + b.length <= 17) { // not a.length because you started with 0
        newTitle.push(b)
      }

      return a + b.length
    }, 0) // start with 0

    return newTitle.join(' ') + '...'
  }

  return title
}

const renderRecipe = r => {
  const li = `
    <li>
      <a class="results__link" href="#${r.recipe_id}">
        <figure class="results__fig">
          <img src="${r.image_url}" alt="Test">
        </figure>
        <div class="results__data">
          <h4 class="results__name">${limitTitle(r.title, 17)}</h4>
          <p class="results__author">${r.publisher}</p>
        </div>
      </a>
    </li>
  `

  elements.searchResList.insertAdjacentHTML('beforeEnd', li)
}

const createBtn = (curPage, btnType) => `
  <button class="btn-inline results__btn--${btnType}" data-goto="${btnType === 'prev' ? curPage - 1 : curPage + 1}">
    <span>Page ${btnType === 'prev' ? curPage - 1 : curPage + 1}</span>
    <svg class="search__icon">
      <use href="img/icons.svg#icon-triangle-${btnType === 'prev' ? 'left' : 'right'}"></use>
    </svg>
  </button>
`

const renderBtn = (curPage, numRes, resPerPage) => {
  const totalPages = Math.ceil(numRes / resPerPage)
  let btn = ''

  if (curPage === 1 && totalPages > 1) {
    // next btn only
    btn = createBtn(curPage, 'next')
  } else if (curPage < totalPages) {
    // both btns
    btn = `
      ${createBtn(curPage, 'prev')}
      ${createBtn(curPage, 'next')}
    `
  } else if (curPage === totalPages && totalPages > 1) {
    // prev btn only
    btn = createBtn(curPage, 'prev')
  }

  elements.searchResPages.insertAdjacentHTML('afterbegin', btn)
}

export const renderResults = (recipes, curPage = 1, resPerPage = 8) => {
  const start = (curPage - 1) * resPerPage
  const end = curPage * resPerPage

  recipes.slice(start, end).forEach(renderRecipe)
  renderBtn(curPage, recipes.length, resPerPage)
}
