import Search from '../js/models/Search'
import Recipe from '../js/models/Recipe'
import { elements, renderLoader, clearLoader } from './views/base'
import * as searchView from './views/searchView'
import * as recipeView from './views/recipeView'
// the same as const searchView = require('searchView')

/*
  Global state of the app
  - Search object (from models/Search.js)
  - current recipe obeject
  - Shopping list object
  - Liked recipes
*/

const state = {}


// ---------------- RECIPE CONTROLLER
// const recipe = new Recipe(46956)
// recipe.getRecipe()
// console.log(recipe)
const controlRecipe = async () => {
  const id = window.location.hash.replace('#', '')

  if (id) {
    // Prepare for UI for changes
    recipeView.clearRecipe()
    renderLoader(elements.recipe)

    if (state.search) {
      searchView.highlightSelected(id)
    }

    // Create new recipe object
    state.recipe = new Recipe(id)

    try {
      // Get recipe
      await state.recipe.getRecipe()
      state.recipe.parseIngredient()

      // Calculate servings and time
      state.recipe.cookingTime()
      state.recipe.serves()

      // Render recipe
      // console.log(state.recipe)
      clearLoader()
      recipeView.renderRecipe(state.recipe)

    } catch (error) {
      alert('Error processing recipe.')
    }
  }
}


// ---------------- SEARCH CONTROLLER
const controlSearch = async () => {
  // 1. get query string from view
  const query = searchView.getInput()

  if (query) {
  // 2. create new search object
    state.search = new Search(query)

  // 3. prepare UI for results (ie: loaders etc)
    searchView.clear()
    renderLoader(elements.searchRes)

    try {
      // 4. search for recipes
      await state.search.getResult()

      // 5. render result on UI
      searchView.renderResults(state.search.result)
    } catch (error) {
      console.log(error)
    } finally {
      clearLoader()
    }
  }
}

elements.searchForm.addEventListener('submit', e => {
  e.preventDefault()
  controlSearch()
})

elements.searchResPages.addEventListener('click', e => {
  const btn = e.target.closest('.btn-inline')

  if (btn) {
    const goToPage = parseInt(btn.dataset.goto, 10) // parseInt can specify base
    searchView.clear()
    searchView.renderResults(state.search.result, goToPage)
  }
})



window.addEventListener('hashchange', controlRecipe)
window.addEventListener('load', controlRecipe)
// ['hashchange', 'load'].forEach(event => window.addEventListener(event, controlRecipe)) // hits error why?
