import axios from 'axios' // import is async by nature
// using babel, import / export will be transpiled to commonjs as well

export default class Search {
  constructor (query) {
    this.query = query
  }

  async getResult() { // why cannot use arrow function here?
    try {
      // console.log('Searching...')
      // const res = await axios(`${proxy}https://www.food2fork.com/api/search?key=${api}&q=${this.query}`)
      const res = await axios(`https://forkify-api.herokuapp.com/api/search?q=${this.query}`)
      this.result = res.data.recipes
    } catch (e) {
      console.log(e)
    }
  }
}
