import axios from 'axios'
import conf from '../config'
export default class Recipe {
  constructor (id) {
    this.id = id
  }

  async getRecipe () {
    try {
      const result = await axios(conf.FOOD2FORK_GET + '/?rId=' + this.id)
      this.title = result.data.recipe.title
      this.author = result.data.recipe.publisher
      this.img = result.data.recipe.image_url
      this.url = result.data.recipe.source_url
      this.ingredients = result.data.recipe.ingredients
    } catch (error) {
      console.log(error)
      window.alert('Something went wrong! :(')
    }
  }

  cookingTime () {
    // estimate roughly 3 ingredients will take 15 min to cook
    this.time = Math.ceil(this.ingredients.length / 3 * 15)
  }

  serves () {
    this.servings = 4
  }

  parseIngredient () {
    const unitsLong = ['tablespoons', 'tablespoon', 'teaspoons', 'teaspoon', 'ounces', 'ounce', 'cups', 'pounds'] // search sequence
    const unitsShort = ['tbsp', 'tbsp', 'tsp', 'tsp', 'oz', 'oz', 'cup', 'pound']
    const units = [...unitsShort, 'kg', 'g']

    const newIngredient = this.ingredients.map(val => {
      // 1) uniform units
      let ingredient = val.toLowerCase()
      unitsLong.forEach((unit, i) => {
        ingredient = ingredient.replace(unit, unitsShort[i])
      })

      // 2) Remove parenthesis
      ingredient.replace(/ *\([^)]*\) */g, ' ')

      //  3) Parse all items
      const arrIng = ingredient.split(' ')
      const unitIndex = arrIng.findIndex(el => units.includes(el))

      let objIng = {
        count: 1,
        unit: '',
        ingredient
      }

      if (unitIndex > -1) { // unit exists
        // 4 1/2 cups = [4, 1/2]
        // 4 cups = [4]
        const arrCount = arrIng.slice(0, unitIndex)

        if (arrCount.length === 1) {
          objIng.count = arrCount[0].replace('-', '+')
        } else {
          objIng.count = eval(arrIng.slice(0, unitIndex).join('+'))
        }

        objIng.unit = arrIng[unitIndex]
        objIng.ingredient = arrIng.slice(unitIndex + 1).join(' ').trim()
      } else if (parseInt(arrIng[0], 10)) { // no unit but first text is number
        objIng.count = parseInt(arrIng[0], 10)
        objIng.ingredient = arrIng.slice(1).join(' ').trim()
      }
      // else if (unitIndex === -1) {
      //   objIng = {
      //     count: 1,
      //     unit: '',
      //     ingredient
      //   }
      // }

      return objIng
    })

    this.ingredients = newIngredient
  }
}
