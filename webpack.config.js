const path = require('path')
const HtmlwebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: ['@babel/polyfill', './src/js/index.js'], // can have more than one entry point
  target: 'node', // without this line, it throw Cannot find 'fs' module
  node: {
    fs: 'empty'
  },
  output: {
    path: path.resolve(__dirname, 'dist'), // have to be absolute path
    filename: 'js/bundle.js'
  },

  devServer: {
    contentBase: './dist' // it's like the root after compile
  },

  plugins: [
    new HtmlwebpackPlugin({
      filname: 'index.html',
      template: './src/index.html'
    })
  ],

  module: {
    rules: [
      {
        test: /\.js$/, // look for all the files with extensions .js
        exclude: /node_modules/, // exclude all js files in node_modules
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  }
}
